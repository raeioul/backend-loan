const express = require("express");
const MongoClient = require("mongodb").MongoClient;
const bodyParser = require("body-parser");
var ObjectID = require("mongodb").ObjectID;
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

MongoClient.connect(
  "mongodb://raeioul:raeioul1@ds337377.mlab.com:37377/loans",
  { useNewUrlParser: true },
  (err, db) => {
    if (err) return console.log(err);

    app.listen(3000, () => {
      console.log("app working on 3000");
    });

    let dbase = db.db("loans");

    app.post("/loan", (req, res, next) => {
      let user = {
        email: req.body.email,
        amount: req.body.amount
      };

      dbase
        .collection("users")
        .find({ email: req.body.email })
        .toArray((err, results) => {
          if (results[0]) {
            const amount = eval(req.body.amount) + eval(results[0].amount);

            if (amount <= 1000) {
              var id = {
                _id: new ObjectID(results[0]._id)
              };

              dbase.collection("users").updateOne(
                id,
                {
                  $set: {
                    amount: amount
                  }
                },
                (err, result) => {
                  if (err) {
                    throw err;
                  }
                }
              );

              dbase
                .collection("users")
                .find()
                .toArray((err, results) => {
                  res.send(
                    {
                      data:results,
                      message: "Amount Modified"
                    }
                  );
                });
            } else { 
              dbase
                .collection("users")
                .find()
                .toArray((err, results) => {
                  res.status(100).send(
                    {
                      data:results,
                      message: "Amount Exceeded"
                    }
                  );
                });
            }
          } else { 
            dbase.collection("users").insertOne(user, (err, result) => {
              if (err) {
                console.log(err);
              }

              dbase
                .collection("users")
                .find()
                .toArray((err, results) => {
                  res.send(
                    {
                      data:results,
                      message: "New User"
                    }
                  );
                });
            });
          }
        });
    });

    app.post("/payments", (req, res, next) => {
      let user = {
        email: req.body.email,
        amount: req.body.amount
      };

      dbase
        .collection("users")
        .find({ email: req.body.email })
        .toArray((err, results) => {
          if (results[0]) {
            const amount = eval(results[0].amount) - eval(req.body.amount);

            if (amount > 0) {
              var id = {
                _id: new ObjectID(results[0]._id)
              };

              dbase.collection("users").updateOne(
                id,
                {
                  $set: {
                    amount: amount
                  }
                },
                (err, result) => {
                  if (err) {
                    throw err;
                  }
                }
              );

              dbase
                .collection("users")
                .find()
                .toArray((err, results) => {
                  res.status(200).send(
                    {
                      data:results,
                      message: "Amount updated sucessfully"
                    }
                  );
                });
            } else {
              dbase
                .collection("users")
                .find()
                .toArray((err, results) => {
                  res.status(200).send(
                    {
                      data:results,
                      message: "Amount exceeds debt"
                    }
                  );
                });
            }
          } else {
            
              dbase
                .collection("users")
                .find()
                .toArray((err, results) => {
                  res.status(200).send(
                    {
                      data:results,
                      message: "We cannot accept the payment"
                    }
                  );
                });
            
          }
        });
    });

    app.get("/users", (req, res, next) => {
      dbase
        .collection("users")
        .find()
        .toArray((err, results) => {
          res.send(results);
        });
    });

    app.post("/information", (req, res, next) => {
      dbase
        .collection("users")
        .find({ email: req.body.email })
        .toArray((err, results) => {
          res.send(results);
        });
    });
  }
);
